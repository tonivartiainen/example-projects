import { generateKeyPairSync } from "crypto";

import {
  PKCS1PublicKey,
  PKCS1PrivateKey,
  encodePKCS1PublicKey,
  decodePKCS1PublicKey,
  encodePKCS1PrivateKey,
  decodePKCS1PrivateKey
} from "@repository/lib/typescript/x509/x509";


export interface JWKPublicKey {
  kty: string
  use?: string
  key_ops?: string
  alg?: string
  kid?: string
  n: bigint
  e: number
}

export interface JWKPrivateKey {
  kty: string
  use?: string
  key_ops?: string
  alg?: string
  kid?: string
  v: number
  n: bigint
  e: number
  d: bigint
  p: bigint
  q: bigint
  dp: bigint
  dq: bigint
  qi: bigint
}

export function decodeJWKPublicKey(buffer: Buffer): JWKPublicKey {

  let pkcs1PublicKey = decodePKCS1PublicKey(buffer);

  return {
    kty: "RSA",
    use: "sig",
    key_ops: "verify",
    alg: "RS256",
    n: pkcs1PublicKey.n,
    e: pkcs1PublicKey.e
  }
}

export function encodeJWKPublicKey(jwkPublicKey: JWKPublicKey): Buffer {

  let pkcs1PublicKey: PKCS1PublicKey = {
    n: jwkPublicKey.n,
    e: jwkPublicKey.e
  }

  return encodePKCS1PublicKey(pkcs1PublicKey)
}

export function decodeJWKPrivateKey(buffer: Buffer): JWKPrivateKey {

  let pkcs1PrivateKey = decodePKCS1PrivateKey(buffer);

  return {
    kty: "RSA",
    use: "sig",
    key_ops: "sign",
    alg: "RS256",
    v: pkcs1PrivateKey.v,
    n: pkcs1PrivateKey.n,
    e: pkcs1PrivateKey.e,
    d: pkcs1PrivateKey.d,
    p: pkcs1PrivateKey.p,
    q: pkcs1PrivateKey.q,
    dp: pkcs1PrivateKey.dp,
    dq: pkcs1PrivateKey.dq,
    qi: pkcs1PrivateKey.qi
  }
}

export function encodeJWKPrivateKey(jwkPrivateKey: JWKPrivateKey): Buffer {

  let pkcs1PrivateKey: PKCS1PrivateKey = {
    v: jwkPrivateKey.v,
    n: jwkPrivateKey.n,
    e: jwkPrivateKey.e,
    d: jwkPrivateKey.d,
    p: jwkPrivateKey.p,
    q: jwkPrivateKey.q,
    dp: jwkPrivateKey.dp,
    dq: jwkPrivateKey.dq,
    qi: jwkPrivateKey.qi
  }

  return encodePKCS1PrivateKey(pkcs1PrivateKey)
}

export function generateJWKKeyPair(): { jwkPublicKey: JWKPublicKey, jwkPrivateKey: JWKPrivateKey } {

  let { publicKey, privateKey } = generateKeyPairSync("rsa", {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: "pkcs1",
      format: "der"
    },
    privateKeyEncoding: {
      type: "pkcs1",
      format: "der"
    }
  })

  return {
    jwkPublicKey: decodeJWKPublicKey(publicKey),
    jwkPrivateKey: decodeJWKPrivateKey(privateKey)
  }
}
