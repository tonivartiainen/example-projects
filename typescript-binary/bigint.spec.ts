import {
  encodeBigUint,
  decodeBigUint,
  encodeBigInt,
  decodeBigInt,
  encodeBigVaruint,
  decodeBigVaruint,
  encodeBigVarint,
  decodeBigVarint
} from "@repository/lib/typescript/binary/bigint"

describe("Binary", function () {

  describe("encodeBigUint/decodeBigUint", function () {

    it("should be able to encode and decode big fixed length unsigned integer", function () {

      let tests = [
        0n,
        1n,
        255n,
        256n,
        65535n,
        65536n,
      ]

      for (let input of tests) {

        let bytes = encodeBigUint(input)

        let output = decodeBigUint(bytes)

        expect(output).toEqual(input)
      }
    })
  })

  describe("encodeBigVaruint/decodeBigVaruint", function () {

    it("should be able to encode and decode big variable length unsigned integer", function () {

      let tests = [
        0n,
        1n,
        255n,
        256n,
        65535n,
        65536n,
      ]

      for (let input of tests) {

        let bytes = encodeBigVaruint(input)

        let [output, length] = decodeBigVaruint(bytes)

        expect(output).toEqual(input)
      }
    })
  })

  describe("encodeBigInt/decodeBigInt", function () {

    it("should be able to encode and decode big fixed length signed integer", function () {

      let tests = [
        0n,
        1n,
        255n,
        256n,
        65535n,
        65536n,
        -1n,
        -255n,
        -256n,
        -65535n,
        -65536n,
      ]

      for (let input of tests) {

        let bytes = encodeBigInt(input)

        let output = decodeBigInt(bytes)

        expect(output).toEqual(input)
      }
    })
  })

  describe("encodeBigVarint/decodeBigVarint", function () {

    it("should be able to encode and decode big variable length signed integer", function () {

      let tests = [
        0n,
        1n,
        255n,
        256n,
        65535n,
        65536n,
        -1n,
        -255n,
        -256n,
        -65535n,
        -65536n,
      ]

      for (let input of tests) {

        let bytes = encodeBigVarint(input)

        let [output, length] = decodeBigVarint(bytes)

        expect(output).toEqual(input)
      }
    })
  })
})
