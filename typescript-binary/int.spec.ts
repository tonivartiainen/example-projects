import {
  encodeUint,
  decodeUint,
  encodeInt,
  decodeInt,
  encodeVaruint,
  decodeVaruint,
  encodeVarint,
  decodeVarint
} from "@repository/lib/typescript/binary/int"

describe("Binary", function () {

  describe("encodeUint/decodeUint", function () {

    it("should be able to encode and decode fixed length unsigned integer", function () {

      let tests = [
        0,
        1,
        255,
        256,
        65535,
        65536,
      ]

      for (let input of tests) {

        let bytes = encodeUint(input)

        let output = decodeUint(bytes)

        expect(output).toEqual(input)
      }
    })
  })

  describe("encodeVaruint/decodeVaruint", function () {

    it("should be able to encode and decode variable length unsigned integer", function () {

      let tests = [
        0,
        1,
        255,
        256,
        65535,
        65536,
      ]

      for (let input of tests) {

        let bytes = encodeVaruint(input)

        let [output, length] = decodeVaruint(bytes)

        expect(output).toEqual(input)
      }
    })
  })

  describe("encodeInt/decodeInt", function () {

    it("should be able to encode and decode fixed length signed integer", function () {

      let tests = [
        0,
        1,
        255,
        256,
        65535,
        65536,
        -1,
        -255,
        -256,
        -65535,
        -65536,
      ]

      for (let input of tests) {

        let bytes = encodeInt(input)

        let output = decodeInt(bytes)

        expect(output).toEqual(input)
      }
    })
  })

  describe("encodeVarint/decodeVarint", function () {

    it("should be able to encode and decode variable length signed integer", function () {

      let tests = [
        0,
        1,
        255,
        256,
        65535,
        65536,
        -1,
        -255,
        -256,
        -65535,
        -65536,
      ]

      for (let input of tests) {

        let bytes = encodeVarint(input)

        let [output, length] = decodeVarint(bytes)

        expect(output).toEqual(input)
      }
    })
  })
})
