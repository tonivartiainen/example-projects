export function encodeBigUint(u: bigint): number[] {

  let b: number[] = []

  b = [Number(u & 0xFFn), ...b]

  u >>= 8n

  while (0 < u) {

    b = [Number(u & 0xFFn), ...b]

    u >>= 8n
  }

  return b
}

export function decodeBigUint(b: number[]): bigint {

  let u = 0n

  let o = 0

  u <<= 8n

  u |= BigInt(b[o] & 0xFF)

  o++

  while (o < b.length) {

    u <<= 8n

    u |= BigInt(b[o] & 0xFF)

    o++
  }

  return u
}

export function encodeBigVaruint(u: bigint): number[] {

  let b: number[] = []

  b = [Number(u & 0x7Fn), ...b]

  u >>= 7n

  while (0 < u) {

    b = [0x80 | Number(u & 0x7Fn), ...b]

    u >>= 7n
  }

  return b
}

export function decodeBigVaruint(b: number[]): [bigint, number] {

  let u = 0n

  let l = 0

  let o = 0

  u <<= 7n

  u |= BigInt(b[o] & 0x7F)

  o++

  l++

  while ((b[o-1] & 0x80) == 0x80) {

    u <<= 7n

    u |= BigInt(b[o] & 0x7F)

    o++

    l++
  }

  return [u, l]
}

export function encodeBigInt(i: bigint): number[] {

  let b: number[] = []

  if (i < 0) {

    let u = -i - 1n

    b = [...b, ...encodeBigUint(u)]

    for (let o in b) {

      b[o] ^= 0xFF
    }

    if ((b[0] & 0x80) == 0x00) {

      b = [0xFF, ...b]
    }
  }
  else {

    let u = i

    b = [...b, ...encodeBigUint(u)]

    if ((b[0] & 0x80) == 0x80) {

      b = [0x00, ...b]
    }
  }

  return b
}

export function decodeBigInt(b: number[]): bigint {

  let i = 0n

  if ((b[0] & 0x80) == 0x80) {

    let u = decodeBigUint(b)

    u = (u ^ ((1n << (8n * BigInt(b.length))) - 1n)) + 1n

    i = -u
  }
  else {

    let u = decodeBigUint(b)

    i = u
  }

  return i
}

export function encodeBigVarint(i: bigint): number[] {

  let b: number[] = []

  if (i < 0) {

		let u = -i - 1n

		b = [...b, ...encodeBigVaruint(u)]

		for (let o in b) {

			b[o] ^= 0x7F
		}

		if ((b[0] & 0x40) == 0x00) {

			b = [0xFF, ...b]
		}
	}
  else {

		let u = i

		b = [...b, ...encodeBigVaruint(u)]

		if ((b[0] & 0x40) == 0x40) {

			b = [0x80, ...b]
		}
	}

	return b
}

export function decodeBigVarint(b: number[]): [bigint, number] {

  let i = 0n

  let l = 0

	if ((b[0] & 0x40) == 0x40) {

		let [u, l] = decodeBigVaruint(b)

		u = (u ^ ((1n << (7n * BigInt(l))) - 1n)) + 1n

		i = -u

	} else {

		let [u, l] = decodeBigVaruint(b)

		i = u
	}

	return [i, l]
}
