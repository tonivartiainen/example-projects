package asn1

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

func MakeSequenceEncoder(sequence ...func() (bytes []byte)) (encode func() (bytes []byte)) {

	encode = func() (bytes []byte) {

		// encode value

		valueBytes := func(value ...func() (bytes []byte)) (valueBytes []byte) {

			for _, encode := range sequence {

				valueBytes = append(valueBytes, encode()...)
			}

			return

		}(sequence...)

		// encode length

		lengthBytes := EncodeLength(len(valueBytes))

		// encode tag

		tagBytes := EncodeTag(Tag{
			class:  0,
			form:   32,
			number: 16,
		})

		bytes = append(bytes, tagBytes...)
		bytes = append(bytes, lengthBytes...)
		bytes = append(bytes, valueBytes...)

		return
	}

	return
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

func MakeSequenceDecoder(sequence ...func(bytes []byte) (bytesLength int)) (decode func(bytes []byte) (bytesLength int)) {

	decode = func(bytes []byte) (bytesLength int) {

		// decode tag

		_, tagBytesLength := DecodeTag(bytes[bytesLength:])

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		length, lengthBytesLength := DecodeLength(bytes[bytesLength:])

		bytesLength += lengthBytesLength

		// decode value

		func(valueBytes []byte) {

			bytesLength := 0

			for _, decode := range sequence {

				bytesLength += decode(valueBytes[bytesLength:])
			}

		}(bytes[bytesLength : bytesLength+length])

		bytesLength += length

		return
	}

	return
}
