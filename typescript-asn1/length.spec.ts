import {
  encodeLength,
  decodeLength
} from "@repository/lib/typescript/asn1/length"

describe("Length", function () {

  describe("encodeLength/decodeLength", function () {

    it("should be able to encode and decode length", function () {

      let input = 8

      let bytes = encodeLength(input)

      let [output, length] = decodeLength(bytes)

      expect(output).toEqual(input)
    })
  })
})
