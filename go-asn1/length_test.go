package asn1

import (
	"testing"
)

func TestLength(t *testing.T) {

	test := 12

	bytes := EncodeLength(test)

	value, _ := DecodeLength(bytes)

	if value != test {

		t.Errorf("Encode %d, Decode %d", test, value)
	}
}
