package binary

import (
	"math/big"
)

func EncodeBigUint(u *big.Int) (b []byte) {

	b = append([]byte{byte(new(big.Int).And(u, big.NewInt(0xFF)).Int64())}, b...)

	u = new(big.Int).Rsh(u, 8)

	for 0 < u.Cmp(big.NewInt(0)) {

		b = append([]byte{byte(new(big.Int).And(u, big.NewInt(0xFF)).Int64())}, b...)

		u = new(big.Int).Rsh(u, 8)
	}

	return
}

func DecodeBigUint(b []byte) (u *big.Int) {

	u = new(big.Int)

	var o int

	u = new(big.Int).Lsh(u, 8)

	u = new(big.Int).Or(u, new(big.Int).And(big.NewInt(int64(b[o])), big.NewInt(0xFF)))

	o++

	for o < len(b) {

		u = new(big.Int).Lsh(u, 8)

		u = new(big.Int).Or(u, new(big.Int).And(big.NewInt(int64(b[o])), big.NewInt(0xFF)))

		o++
	}

	return
}

func EncodeBigVaruint(u *big.Int) (b []byte) {

	b = append([]byte{byte(new(big.Int).And(u, big.NewInt(0x7F)).Int64())}, b...)

	u = new(big.Int).Rsh(u, 7)

	for 0 < u.Cmp(big.NewInt(0)) {

		b = append([]byte{byte(new(big.Int).Or(big.NewInt(0x80), new(big.Int).And(u, big.NewInt(0x7F))).Int64())}, b...)

		u = new(big.Int).Rsh(u, 7)
	}

	return
}

func DecodeBigVaruint(b []byte) (u *big.Int, l int) {

	u = new(big.Int)

	var o int

	u = new(big.Int).Lsh(u, 7)

	u = new(big.Int).Or(u, new(big.Int).And(big.NewInt(int64(b[o])), big.NewInt(0x7F)))

	o++

	l++

	for b[o-1]&0x80 == 0x80 {

		u = new(big.Int).Lsh(u, 7)

		u = new(big.Int).Or(u, new(big.Int).And(big.NewInt(int64(b[o])), big.NewInt(0x7F)))

		o++

		l++
	}

	return
}

func EncodeBigInt(i *big.Int) (b []byte) {

	if i.Cmp(big.NewInt(0)) < 0 {

		u := new(big.Int).Sub(new(big.Int).Neg(i), big.NewInt(1))

		b = append(b, EncodeBigUint(u)...)

		for o := range b {

			b[o] ^= 0xFF
		}

		if b[0]&0x80 == 0x00 {

			b = append([]byte{0xFF}, b...)
		}

	} else {

		u := new(big.Int).Set(i)

		b = append(b, EncodeBigUint(u)...)

		if b[0]&0x80 == 0x80 {

			b = append([]byte{0x00}, b...)
		}
	}

	return
}

func DecodeBigInt(b []byte) (i *big.Int) {

	if b[0]&128 == 128 {

		u := DecodeBigUint(b)

		u = new(big.Int).Add(new(big.Int).Xor(u, new(big.Int).Sub(new(big.Int).Lsh(big.NewInt(1), uint(8*len(b))), big.NewInt(1))), big.NewInt(1))

		i = new(big.Int).Neg(u)

	} else {

		u := DecodeBigUint(b)

		i = new(big.Int).Set(u)
	}

	return
}

func EncodeBigVarint(i *big.Int) (b []byte) {

	if i.Cmp(big.NewInt(0)) < 0 {

		u := new(big.Int).Sub(new(big.Int).Neg(i), big.NewInt(1))

		b = append(b, EncodeBigVaruint(u)...)

		for o := range b {

			b[o] ^= 0x7F
		}

		if b[0]&0x40 == 0x00 {

			b = append([]byte{0xFF}, b...)
		}

	} else {

		u := new(big.Int).Set(i)

		b = append(b, EncodeBigVaruint(u)...)

		if b[0]&0x40 == 0x40 {

			b = append([]byte{0x80}, b...)
		}
	}

	return
}

func DecodeBigVarint(b []byte) (i *big.Int, l int) {

	if b[0]&0x40 == 0x40 {

		u := new(big.Int)

		u, l = DecodeBigVaruint(b)

		u = new(big.Int).Add(new(big.Int).Xor(u, new(big.Int).Sub(new(big.Int).Lsh(big.NewInt(1), uint(7*l)), big.NewInt(1))), big.NewInt(1))

		i = new(big.Int).Neg(u)

	} else {

		u := new(big.Int)

		u, l = DecodeBigVaruint(b)

		i = new(big.Int).Set(u)
	}

	return
}
