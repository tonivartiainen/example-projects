package asn1

import (
	"testing"
)

func TestTag(t *testing.T) {

	test := Tag{
		class:  0,
		form:   0,
		number: 1,
	}

	bytes := EncodeTag(test)

	value, _ := DecodeTag(bytes)

	if value != test {

		t.Errorf("Encode %v, Decode %v", test, value)
	}
}
