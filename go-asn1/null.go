package asn1

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

func MakeNullEncoder(null interface{}) (encode func() (bytes []byte)) {

	encode = func() (bytes []byte) {

		// encode value

		valueBytes := func(value interface{}) (valueBytes []byte) {

			return

		}(null)

		// encode length

		lengthBytes := EncodeLength(len(valueBytes))

		// encode tag

		tagBytes := EncodeTag(Tag{
			class:  0,
			form:   0,
			number: 5,
		})

		bytes = append(bytes, tagBytes...)
		bytes = append(bytes, lengthBytes...)
		bytes = append(bytes, valueBytes...)

		return
	}

	return
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

func MakeNullDecoder(null *interface{}) (decode func(bytes []byte) (bytesLength int)) {

	decode = func(bytes []byte) (bytesLength int) {

		// decode tag

		_, tagBytesLength := DecodeTag(bytes[bytesLength:])

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		length, lengthBytesLength := DecodeLength(bytes[bytesLength:])

		bytesLength += lengthBytesLength

		// decode value

		value := func(valueBytes []byte) (value interface{}) {

			return

		}(bytes[bytesLength : bytesLength+length])

		bytesLength += length

		// set value

		null = &value

		return
	}

	return
}
