package binary

import (
	"testing"
)

func TestUint(t *testing.T) {

	s := []uint64{
		0,
		1,
		255,
		256,
		65535,
		65536,
	}

	for _, u := range s {

		b := EncodeUint(u)

		du := DecodeUint(b)

		if du != u {

			t.Errorf("EncodeUint %d, DecodeUint %d", u, du)
		}
	}
}

func TestVaruint(t *testing.T) {

	s := []uint64{
		0,
		1,
		255,
		256,
		65535,
		65536,
	}

	for _, u := range s {

		b := EncodeVaruint(u)

		du, _ := DecodeVaruint(b)

		if du != u {

			t.Errorf("EncodeVaruint %d, DecodeVaruint %d", u, du)
		}
	}
}

func TestInt(t *testing.T) {

	s := []int64{
		0,
		1,
		255,
		256,
		65535,
		65536,
		-1,
		-255,
		-256,
		-65535,
		-65536,
	}

	for _, i := range s {

		b := EncodeInt(i)

		di := DecodeInt(b)

		if di != i {

			t.Errorf("EncodeInt %d, DecodeInt %d", i, di)
		}
	}
}

func TestVarint(t *testing.T) {

	s := []int64{
		0,
		1,
		255,
		256,
		65535,
		65536,
		-1,
		-255,
		-256,
		-65535,
		-65536,
	}

	for _, i := range s {

		b := EncodeVarint(i)

		di, _ := DecodeVarint(b)

		if di != i {

			t.Errorf("EncodeVarint %d, DecodeVarint %d", i, di)
		}
	}
}
