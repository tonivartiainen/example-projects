package asn1

import (
	"regexp"
	"strconv"
	"strings"
)

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

func MakeBitStringEncoder(bitString string) (encode func() (bytes []byte)) {

	encode = func() (bytes []byte) {

		// encode value

		valueBytes := func(value string) (valueBytes []byte) {

			subvalues := regexp.MustCompile(`[0,1]{1,8}`).FindAllString(value, -1)

			for i := 0; i < len(subvalues)-1; i++ {

				subvalue := subvalues[i]

				parsedSubvalue, _ := strconv.ParseUint(subvalue, 2, 64)

				valueByte := byte(parsedSubvalue)

				valueBytes = append(valueBytes, valueByte)
			}

			subvalue := subvalues[len(subvalues)-1]

			paddingByte := byte(8 - len(subvalue))

			valueBytes = append([]byte{paddingByte}, valueBytes...)

			subvalue = subvalue + strings.Repeat("0", 8-len(subvalue))

			parsedSubvalue, _ := strconv.ParseUint(subvalue, 2, 8)

			valueByte := byte(parsedSubvalue)

			valueBytes = append(valueBytes, valueByte)

			return

		}(bitString)

		// encode length

		lengthBytes := EncodeLength(len(valueBytes))

		// encode tag

		tagBytes := EncodeTag(Tag{
			class:  0,
			form:   0,
			number: 3,
		})

		bytes = append(bytes, tagBytes...)
		bytes = append(bytes, lengthBytes...)
		bytes = append(bytes, valueBytes...)

		return
	}

	return
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

func MakeBitStringDecoder(bitString *string) (decode func(bytes []byte) (bytesLength int)) {

	decode = func(bytes []byte) (bytesLength int) {

		// decode tag

		_, tagBytesLength := DecodeTag(bytes[bytesLength:])

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		length, lengthBytesLength := DecodeLength(bytes[bytesLength:])

		bytesLength += lengthBytesLength

		// decode value

		value := func(valueBytes []byte) (value string) {

			var subvalues []string

			valueByte := valueBytes[0]

			padding := int(valueByte)

			for i := 1; i < len(valueBytes)-1; i++ {

				valueByte := valueBytes[i]

				subvalue := strconv.FormatUint(uint64(valueByte), 2)

				subvalue = strings.Repeat("0", 8-len(subvalue)) + subvalue

				subvalues = append(subvalues, subvalue)
			}

			valueByte = valueBytes[len(valueBytes)-1]

			subvalue := strconv.FormatUint(uint64(valueByte>>padding), 2)

			subvalues = append(subvalues, subvalue)

			value = strings.Join(subvalues, "")

			return

		}(bytes[bytesLength : bytesLength+length])

		bytesLength += length

		// set value

		*bitString = value

		return
	}

	return
}
