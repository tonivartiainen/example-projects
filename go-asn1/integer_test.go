package asn1

import (
	"testing"
)

func TestInteger(t *testing.T) {

	var in int64 = 1234567890

	encode := MakeIntegerEncoder(in)

	bytes := encode()

	var out int64

	decode := MakeIntegerDecoder(&out)

	decode(bytes)

	if in != out {

		t.Errorf("Encode %d, Decode %d", in, out)
	}
}
