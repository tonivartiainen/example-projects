import {
  encodeVaruint,
  decodeVaruint
} from "@repository/lib/typescript/binary/int"

export interface Tag {
	class:  number
	form:   number
	number: number
}

export function encodeTag(tag: Tag): number[] {

  let tagBytes: number[] = []

	let tagByte = 0

	tagByte |= tag.class & 0xC0

	tagByte |= tag.form & 0x20

	if (tag.number < 0x1F) {

		tagByte |= tag.number & 0x1F

		tagBytes = [...tagBytes, tagByte]

	} else {

		tagByte |= 0x1F

		tagBytes = [...tagBytes, tagByte]

		let tagNumberBytes = encodeVaruint(tag.number)

		tagBytes = [...tagBytes, ...tagNumberBytes]
	}

	return tagBytes
}

export function decodeTag(bytes: number[]): [Tag, number] {

  let tag: Tag = {
    class:  0,
    form:   0,
    number: 0
  }

  let tagBytesLength = 0

	let tagByte = bytes[tagBytesLength]

	tagBytesLength++

	tag.class = tagByte & 0xC0

	tag.form = tagByte & 0x20

	if ((tagByte & 0x1F) == 0x1F) {

		let [tagNumber, tagNumberBytesLength] = decodeVaruint(bytes.slice(tagBytesLength))

		tag.number = tagNumber

		tagBytesLength += tagNumberBytesLength

	} else {

		tag.number = tagByte & 0x1F
	}

	return [tag, tagBytesLength]
}
