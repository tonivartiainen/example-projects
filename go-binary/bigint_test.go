package binary

import (
	"math/big"
	"testing"
)

func TestBigUint(t *testing.T) {

	s := []*big.Int{
		big.NewInt(0),
		big.NewInt(1),
		big.NewInt(255),
		big.NewInt(256),
		big.NewInt(65535),
		big.NewInt(65536),
	}

	for _, u := range s {

		b := EncodeBigUint(u)

		du := DecodeBigUint(b)

		if du.Cmp(u) != 0 {

			t.Errorf("EncodeBigUint %d, DecodeBigUint %d", u, du)
		}
	}
}

func TestBigVaruint(t *testing.T) {

	s := []*big.Int{
		big.NewInt(0),
		big.NewInt(1),
		big.NewInt(255),
		big.NewInt(256),
		big.NewInt(65535),
		big.NewInt(65536),
	}

	for _, u := range s {

		b := EncodeBigVaruint(u)

		du, _ := DecodeBigVaruint(b)

		if du.Cmp(u) != 0 {

			t.Errorf("EncodeBigVaruint %d, DecodeBigVaruint %d", u, du)
		}
	}
}

func TestBigInt(t *testing.T) {

	s := []*big.Int{
		big.NewInt(0),
		big.NewInt(1),
		big.NewInt(255),
		big.NewInt(256),
		big.NewInt(65535),
		big.NewInt(65536),
		big.NewInt(-1),
		big.NewInt(-255),
		big.NewInt(-256),
		big.NewInt(-65535),
		big.NewInt(-65536),
	}

	for _, i := range s {

		b := EncodeBigInt(i)

		di := DecodeBigInt(b)

		if di.Cmp(i) != 0 {

			t.Errorf("EncodeBigInt %d, DecodeBigInt %d", i, di)
		}
	}
}

func TestBigVarint(t *testing.T) {

	s := []*big.Int{
		big.NewInt(0),
		big.NewInt(1),
		big.NewInt(255),
		big.NewInt(256),
		big.NewInt(65535),
		big.NewInt(65536),
		big.NewInt(-1),
		big.NewInt(-255),
		big.NewInt(-256),
		big.NewInt(-65535),
		big.NewInt(-65536),
	}

	for _, i := range s {

		b := EncodeBigVarint(i)

		di, _ := DecodeBigVarint(b)

		if di.Cmp(i) != 0 {

			t.Errorf("EncodeBigVarint %d, DecodeBigVarint %d", i, di)
		}
	}
}
