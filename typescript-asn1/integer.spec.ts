import {
  makeIntegerEncoder,
  makeIntegerDecoder
} from "@repository/lib/typescript/asn1/integer"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

describe("Integer", function () {

  describe("encode/decode", function () {

    it("should be able to encode and decode integer", function () {

      let input = new Ref<number>(12345)

      let encode = makeIntegerEncoder(input)

      let bytes = encode()

      let output = new Ref<number>()

      let decode = makeIntegerDecoder(output)

      decode(bytes)

      expect(output).toEqual(input)
    })
  })
})
