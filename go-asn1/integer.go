package asn1

import (
	"repository/lib/go/binary"
)

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

func MakeIntegerEncoder(integer int64) (encode func() (bytes []byte)) {

	encode = func() (bytes []byte) {

		// encode value

		valueBytes := func(value int64) (valueBytes []byte) {

			valueBytes = binary.EncodeInt(value)

			return

		}(integer)

		// encode length

		lengthBytes := EncodeLength(len(valueBytes))

		// encode tag

		tagBytes := EncodeTag(Tag{
			class:  0,
			form:   0,
			number: 2,
		})

		bytes = append(bytes, tagBytes...)
		bytes = append(bytes, lengthBytes...)
		bytes = append(bytes, valueBytes...)

		return
	}

	return
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

func MakeIntegerDecoder(integer *int64) (decode func(bytes []byte) (bytesLength int)) {

	decode = func(bytes []byte) (bytesLength int) {

		// decode tag

		_, tagBytesLength := DecodeTag(bytes[bytesLength:])

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		length, lengthBytesLength := DecodeLength(bytes[bytesLength:])

		bytesLength += lengthBytesLength

		// decode value

		value := func(valueBytes []byte) (value int64) {

			value = binary.DecodeInt(valueBytes)

			return

		}(bytes[bytesLength : bytesLength+length])

		bytesLength += length

		// set value

		*integer = value

		return
	}

	return
}
