package asn1

import (
	"math/big"
	"testing"
)

func TestBigInteger(t *testing.T) {

	in := big.NewInt(1234567890)

	encode := MakeBigIntegerEncoder(in)

	bytes := encode()

	out := new(big.Int)

	decode := MakeBigIntegerDecoder(&out)

	decode(bytes)

	if in.Cmp(out) != 0 {

		t.Errorf("Encode %d, Decode %d", in, out)
	}
}
