package asn1

import (
	"repository/lib/go/binary"
)

type Tag struct {
	class  uint64
	form   uint64
	number uint64
}

func EncodeTag(tag Tag) (tagBytes []byte) {

	var tagByte byte

	tagByte |= byte(tag.class & 0xC0)

	tagByte |= byte(tag.form & 0x20)

	if tag.number < 0x1F {

		tagByte |= byte(tag.number & 0x1F)

		tagBytes = append(tagBytes, tagByte)

	} else {

		tagByte |= byte(0x1F)

		tagBytes = append(tagBytes, tagByte)

		tagNumberBytes := binary.EncodeVaruint(tag.number)

		tagBytes = append(tagBytes, tagNumberBytes...)
	}

	return
}

func DecodeTag(bytes []byte) (tag Tag, tagBytesLength int) {

	tagByte := bytes[tagBytesLength]

	tagBytesLength++

	tag.class = uint64(tagByte & 0xC0)

	tag.form = uint64(tagByte & 0x20)

	if tagByte&0x1F == 0x1F {

		tagNumber, tagNumberBytesLength := binary.DecodeVaruint(bytes[tagBytesLength:])

		tag.number = tagNumber

		tagBytesLength += tagNumberBytesLength

	} else {

		tag.number = uint64(tagByte & 0x1F)
	}

	return
}
