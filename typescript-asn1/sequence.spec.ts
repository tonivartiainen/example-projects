import {
  makeSequenceEncoder,
  makeSequenceDecoder
} from "@repository/lib/typescript/asn1/sequence"

import {
  makeIntegerEncoder,
  makeIntegerDecoder
} from "@repository/lib/typescript/asn1/integer"

import {
  makeBigIntegerEncoder,
  makeBigIntegerDecoder
} from "@repository/lib/typescript/asn1/biginteger"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

describe("Sequence", function () {

  describe("encode/decode", function () {

    it("should be able to encode and decode sequence", function () {

      let input = {
        n: new Ref<number>(100),
        e: new Ref<bigint>(200),
      }

      let encode = makeSequenceEncoder(
        makeIntegerEncoder(input.n),
        makeBigIntegerEncoder(input.e),
      )

      let bytes = encode()

      let output = {
        n: new Ref<number>(),
        e: new Ref<number>(),
      }

      let decode = makeSequenceDecoder(
        makeIntegerDecoder(output.n),
        makeIntegerDecoder(output.e),
      )

      decode(bytes)

      expect(output).toEqual(input)
    })
  })
})
