import {
  makeObjectIdentifierEncoder,
  makeObjectIdentifierDecoder
} from "@repository/lib/typescript/asn1/objectidentifier"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

describe("ObjectIdentifier", function () {

  describe("encode/decode", function () {

    it("should be able to encode and decode object identifier", function () {

      let input = new Ref<string>("1.2.840.113549.1.1")

      let encode = makeObjectIdentifierEncoder(input)

      let bytes = encode()

      let output = new Ref<string>()

      let decode = makeObjectIdentifierDecoder(output)

      decode(bytes)

      expect(output).toEqual(input)
    })
  })
})
