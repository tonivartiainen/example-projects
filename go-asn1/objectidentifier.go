package asn1

import (
	"repository/lib/go/binary"
	"strconv"
	"strings"
)

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

func MakeObjectIdentifierEncoder(objectIdentifier string) (encode func() (bytes []byte)) {

	encode = func() (bytes []byte) {

		// encode value

		valueBytes := func(value string) (valueBytes []byte) {

			subvalues := strings.Split(value, ".")

			var valueByte byte

			subvalue := subvalues[0]

			parsedSubvalue, _ := strconv.ParseUint(subvalue, 10, 64)

			valueByte += byte(parsedSubvalue * 40)

			subvalue = subvalues[1]

			parsedSubvalue, _ = strconv.ParseUint(subvalue, 10, 64)

			valueByte += byte(parsedSubvalue)

			valueBytes = append(valueBytes, valueByte)

			for i := 2; i < len(subvalues); i++ {

				subvalue = subvalues[i]

				parsedSubvalue, _ = strconv.ParseUint(subvalue, 10, 64)

				subvalueBytes := binary.EncodeVaruint(parsedSubvalue)

				valueBytes = append(valueBytes, subvalueBytes...)
			}

			return

		}(objectIdentifier)

		// encode length

		lengthBytes := EncodeLength(len(valueBytes))

		// encode tag

		tagBytes := EncodeTag(Tag{
			class:  0,
			form:   0,
			number: 6,
		})

		bytes = append(bytes, tagBytes...)
		bytes = append(bytes, lengthBytes...)
		bytes = append(bytes, valueBytes...)

		return
	}

	return
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

func MakeObjectIdentifierDecoder(objectIdentifier *string) (decode func(bytes []byte) (bytesLength int)) {

	decode = func(bytes []byte) (bytesLength int) {

		// decode tag

		_, tagBytesLength := DecodeTag(bytes[bytesLength:])

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		length, lengthBytesLength := DecodeLength(bytes[bytesLength:])

		bytesLength += lengthBytesLength

		// decode value

		value := func(valueBytes []byte) (value string) {

			var subvalues []string

			valueByte := valueBytes[0]

			subvalue := strconv.FormatUint(uint64(valueByte/40-valueByte%40/40), 10)

			subvalues = append(subvalues, subvalue)

			subvalue = strconv.FormatUint(uint64(valueByte%40), 10)

			subvalues = append(subvalues, subvalue)

			for i := 1; i < len(valueBytes); i++ {

				decodedSubvalue, decodedSubvalueBytesLength := binary.DecodeVaruint(valueBytes[i:])

				subvalue = strconv.FormatUint(uint64(decodedSubvalue), 10)

				i += int(decodedSubvalueBytesLength) - 1

				subvalues = append(subvalues, subvalue)
			}

			value = strings.Join(subvalues, ".")

			return

		}(bytes[bytesLength : bytesLength+length])

		bytesLength += length

		// set value

		*objectIdentifier = value

		return
	}

	return
}
