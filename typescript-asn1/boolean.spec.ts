import {
  makeBooleanEncoder,
  makeBooleanDecoder
} from "@repository/lib/typescript/asn1/boolean"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

describe("Boolean", function () {

  describe("encode/decode", function () {

    it("should be able to encode and decode boolean", function () {

      let input = new Ref<boolean>(true)

      let encode = makeBooleanEncoder(input)

      let bytes = encode()

      let output = new Ref<boolean>()

      let decode = makeBooleanDecoder(output)

      decode(bytes)

      expect(output).toEqual(input)
    })
  })
})
