import {
  encodeBigInt,
  decodeBigInt
} from "@repository/lib/typescript/binary/bigint"

import {
  encodeTag,
  decodeTag
} from "@repository/lib/typescript/asn1/tag"

import {
  encodeLength,
  decodeLength
} from "@repository/lib/typescript/asn1/length"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

export function makeBigIntegerEncoder(ref: Ref<bigint>): () => number[] {

	let encode = (): number[] => {

    let bytes: number[] = []

		// encode value

		let valueBytes = function(ref: Ref<bigint>): number[] {

      // get value

      let value = ref.value

			let valueBytes = encodeBigInt(value)

			return valueBytes

		}(ref)

		// encode length

		let lengthBytes = encodeLength(valueBytes.length)

		// encode tag

		let tagBytes = encodeTag({
			class:  0,
			form:   0,
			number: 2,
		})

		bytes = [...bytes, ...tagBytes]
		bytes = [...bytes, ...lengthBytes]
		bytes = [...bytes, ...valueBytes]

		return bytes
	}

	return encode
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

export function makeBigIntegerDecoder(ref: Ref<bigint>): (bytes: number[]) => number {

  let bytesLength = 0

	let decode = (bytes: number[]): number => {

		// decode tag

		let [tag, tagBytesLength] = decodeTag(bytes.slice(bytesLength))

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		let [length, lengthBytesLength] = decodeLength(bytes.slice(bytesLength))

		bytesLength += lengthBytesLength

		// decode value

		let value = function(valueBytes: number[]): bigint {

			let value = decodeBigInt(valueBytes)

			return value

		}(bytes.slice(bytesLength, bytesLength + length))

		bytesLength += length

		// set value

		ref.value = value

		return bytesLength
	}

	return decode
}
