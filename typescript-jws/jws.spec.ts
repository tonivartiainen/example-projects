import {
  createToken,
  verifyToken
} from "@repository/lib/typescript/jws/jws"

import {
  generateJWKKeyPair
} from "@repository/lib/typescript/jwk/jwk"

describe("JWS", function () {

  describe("createToken/verifyToken", function () {

    it("should be able to create and verify JWT token", function () {

      let { jwkPublicKey, jwkPrivateKey } = generateJWKKeyPair()

      let token = createToken({ alg: "RS256", jwk: jwkPublicKey }, { iss: "test" }, jwkPrivateKey)

      console.log(token)

      expect(verifyToken(token)).toEqual({ iss: "test" })
    })
  })
})
