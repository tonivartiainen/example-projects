package binary

func EncodeUint(u uint64) (b []byte) {

	b = append([]byte{byte(u & 0xFF)}, b...)

	u >>= 8

	for 0 < u {

		b = append([]byte{byte(u & 0xFF)}, b...)

		u >>= 8
	}

	return
}

func DecodeUint(b []byte) (u uint64) {

	var o int

	u <<= 8

	u |= uint64(b[o] & 0xFF)

	o++

	for o < len(b) {

		u <<= 8

		u |= uint64(b[o] & 0xFF)

		o++
	}

	return
}

func EncodeVaruint(u uint64) (b []byte) {

	b = append([]byte{byte(0x00 | u&0x7F)}, b...)

	u >>= 7

	for 0 < u {

		b = append([]byte{byte(0x80 | u&0x7F)}, b...)

		u >>= 7
	}

	return
}

func DecodeVaruint(b []byte) (u uint64, l int) {

	var o int

	u <<= 7

	u |= uint64(b[o] & 0x7F)

	o++

	l++

	for b[o-1]&0x80 == 0x80 {

		u <<= 7

		u |= uint64(b[o] & 0x7F)

		o++

		l++
	}

	return
}

func EncodeInt(i int64) (b []byte) {

	if i < 0 {

		u := uint64(-i - 1)

		b = append(b, EncodeUint(u)...)

		for o := range b {

			b[o] ^= 0xFF
		}

		if b[0]&0x80 == 0x00 {

			b = append([]byte{0xFF}, b...)
		}

	} else {

		u := uint64(i)

		b = append(b, EncodeUint(u)...)

		if b[0]&0x80 == 0x80 {

			b = append([]byte{0x00}, b...)
		}
	}

	return
}

func DecodeInt(b []byte) (i int64) {

	if b[0]&0x80 == 0x80 {

		u := DecodeUint(b)

		u = (u ^ ((1 << (8 * len(b))) - 1)) + 1

		i = int64(-u)

	} else {

		u := DecodeUint(b)

		i = int64(u)
	}

	return
}

func EncodeVarint(i int64) (b []byte) {

	if i < 0 {

		u := uint64(-i - 1)

		b = append(b, EncodeVaruint(u)...)

		for o := range b {

			b[o] ^= 0x7F
		}

		if b[0]&0x40 == 0x00 {

			b = append([]byte{0xFF}, b...)
		}

	} else {

		u := uint64(i)

		b = append(b, EncodeVaruint(u)...)

		if b[0]&0x40 == 0x40 {

			b = append([]byte{0x80}, b...)
		}
	}

	return
}

func DecodeVarint(b []byte) (i int64, l int) {

	if b[0]&0x40 == 0x40 {

		var u uint64

		u, l = DecodeVaruint(b)

		u = (u ^ ((1 << (7 * l)) - 1)) + 1

		i = int64(-u)

	} else {

		var u uint64

		u, l = DecodeVaruint(b)

		i = int64(u)
	}

	return
}
