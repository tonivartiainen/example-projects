export function encodeUint(u: number): number[] {

  let b: number[] = []

  b = [u & 0xFF, ...b]

  u >>>= 8

  while (0 < u) {

    b = [u & 0xFF, ...b]

    u >>>= 8
  }

  return b
}

export function decodeUint(b: number[]): number {

  let u = 0

  let o = 0

  u <<= 8

  u |= b[o] & 0xFF

  o++

  while (o < b.length) {

    u <<= 8

    u |= b[o] & 0xFF

    u >>>= 0

    o++
  }

  return u
}

export function encodeVaruint(u: number): number[] {

  let b: number[] = []

  b = [u & 0x7F, ...b]

  u >>>= 7

  while (0 < u) {

    b = [0x80 | (u & 0x7F), ...b]

    u >>>= 7
  }

  return b
}

export function decodeVaruint(b: number[]): [number, number] {

  let u = 0

  let l = 0

  let o = 0

  u <<= 7

  u |= b[o] & 0x7F

  o++

  l++

  while ((b[o-1] & 0x80) == 0x80) {

    u <<= 7

    u |= b[o] & 0x7F

    u >>>= 0

    o++

    l++
  }

  return [u, l]
}

export function encodeInt(i: number): number[] {

  let b: number[] = []

  if (i < 0) {

    let u = -i - 1

    b = [...b, ...encodeUint(u)]

    for (let o in b) {

      b[o] ^= 0xFF
    }

    if ((b[0] & 0x80) == 0x00) {

      b = [0xFF, ...b]
    }
  }
  else {

    let u = i

    b = [...b, ...encodeUint(u)]

    if ((b[0] & 0x80) == 0x80) {

      b = [0x00, ...b]
    }
  }

  return b
}

export function decodeInt(b: number[]): number {

  let i = 0

  if ((b[0] & 0x80) == 0x80) {

    let u = decodeUint(b)

    u = (u ^ ((1 << (8 * b.length)) - 1)) + 1

    i = -u
  }
  else {

    let u = decodeUint(b)

    i = u
  }

  return i
}

export function encodeVarint(i: number): number[] {

  let b: number[] = []

  if (i < 0) {

		let u = -i - 1

		b = [...b, ...encodeVaruint(u)]

		for (let o in b) {

			b[o] ^= 0x7F
		}

		if ((b[0] & 0x40) == 0x00) {

			b = [0xFF, ...b]
		}
	}
  else {

		let u = i

		b = [...b, ...encodeVaruint(u)]

		if ((b[0] & 0x40) == 0x40) {

			b = [0x80, ...b]
		}
	}

	return b
}

export function decodeVarint(b: number[]): [number, number] {

  let i = 0

  let l = 0

	if ((b[0] & 0x40) == 0x40) {

		let [u, l] = decodeVaruint(b)

		u = (u ^ ((1 << (7 * l)) - 1)) + 1

		i = -u

	} else {

		let [u, l] = decodeVaruint(b)

		i = u
	}

	return [i, l]
}
