package asn1

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

func MakeBooleanEncoder(boolean bool) (encode func() (bytes []byte)) {

	encode = func() (bytes []byte) {

		// encode value

		valueBytes := func(value bool) (valueBytes []byte) {

			if value == true {

				valueByte := byte(0xFF)

				valueBytes = append(valueBytes, valueByte)

			} else {

				valueByte := byte(0x00)

				valueBytes = append(valueBytes, valueByte)
			}

			return

		}(boolean)

		// encode length

		lengthBytes := EncodeLength(len(valueBytes))

		// encode tag

		tagBytes := EncodeTag(Tag{
			class:  0,
			form:   0,
			number: 1,
		})

		bytes = append(bytes, tagBytes...)
		bytes = append(bytes, lengthBytes...)
		bytes = append(bytes, valueBytes...)

		return
	}

	return
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

func MakeBooleanDecoder(boolean *bool) (decode func(bytes []byte) (bytesLength int)) {

	decode = func(bytes []byte) (bytesLength int) {

		// decode tag

		_, tagBytesLength := DecodeTag(bytes[bytesLength:])

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		length, lengthBytesLength := DecodeLength(bytes[bytesLength:])

		bytesLength += lengthBytesLength

		// decode value

		value := func(valueBytes []byte) (value bool) {

			valueByte := valueBytes[0]

			if valueByte == 0xFF {

				value = true

			} else {

				value = false
			}

			return

		}(bytes[bytesLength : bytesLength+length])

		bytesLength += length

		// set value

		*boolean = value

		return
	}

	return
}
