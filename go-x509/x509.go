package x509

import (
	"repository/lib/go/asn1"
)

type PKCS1PublicKey struct {
	n int64
	e int64
}

func EncodePKCS1PublicKey(pkc1PublicKey PKCS1PublicKey) (bytes []byte) {

	encode := asn1.MakeSequenceEncoder(
		asn1.MakeIntegerEncoder(pkc1PublicKey.n),
		asn1.MakeIntegerEncoder(pkc1PublicKey.e),
	)

	bytes = encode()

	return
}

func DecodePKCS1PublicKey(bytes []byte) (pkc1PublicKey PKCS1PublicKey) {

	pkc1PublicKey = PKCS1PublicKey{}

	decode := asn1.MakeSequenceDecoder(
		asn1.MakeIntegerDecoder(&pkc1PublicKey.n),
		asn1.MakeIntegerDecoder(&pkc1PublicKey.e),
	)

	decode(bytes)

	return
}
