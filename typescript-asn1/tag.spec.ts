import {
  Tag,
  encodeTag,
  decodeTag
} from "@repository/lib/typescript/asn1/tag"

describe("Tag", function () {

  describe("encodeTag/decodeTag", function () {

    it("should be able to encode and decode tag", function () {

      let input: Tag = {
        class:  0,
        form:   0,
        number: 1,
      }

      let bytes = encodeTag(input)

      let [output, length] = decodeTag(bytes)

      expect(output).toEqual(input)
    })
  })
})
