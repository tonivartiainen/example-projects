package asn1

import (
	"math/big"
	"repository/lib/go/binary"
)

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

func MakeBigIntegerEncoder(bigInteger *big.Int) (encode func() (bytes []byte)) {

	encode = func() (bytes []byte) {

		// encode value

		valueBytes := func(value *big.Int) (valueBytes []byte) {

			valueBytes = binary.EncodeBigInt(value)

			return

		}(bigInteger)

		// encode length

		lengthBytes := EncodeLength(len(valueBytes))

		// encode tag

		tagBytes := EncodeTag(Tag{
			class:  0,
			form:   0,
			number: 2,
		})

		bytes = append(bytes, tagBytes...)
		bytes = append(bytes, lengthBytes...)
		bytes = append(bytes, valueBytes...)

		return
	}

	return
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

func MakeBigIntegerDecoder(bigInteger **big.Int) (decode func(bytes []byte) (bytesLength int)) {

	decode = func(bytes []byte) (bytesLength int) {

		// decode tag

		_, tagBytesLength := DecodeTag(bytes[bytesLength:])

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		length, lengthBytesLength := DecodeLength(bytes[bytesLength:])

		bytesLength += lengthBytesLength

		// decode value

		value := func(valueBytes []byte) (value *big.Int) {

			value = binary.DecodeBigInt(valueBytes)

			return

		}(bytes[bytesLength : bytesLength+length])

		bytesLength += length

		// set value

		*bigInteger = value

		return
	}

	return
}
