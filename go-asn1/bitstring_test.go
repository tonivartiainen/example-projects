package asn1

import (
	"testing"
)

func TestBitString(t *testing.T) {

	in := "101010101010"

	encode := MakeBitStringEncoder(in)

	bytes := encode()

	out := ""

	decode := MakeBitStringDecoder(&out)

	decode(bytes)

	if in != out {

		t.Errorf("Encode %s, Decode %s", in, out)
	}
}
