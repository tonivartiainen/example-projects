import {
  encodeInt,
  decodeInt
} from "@repository/lib/typescript/binary/int"

import {
  encodeTag,
  decodeTag
} from "@repository/lib/typescript/asn1/tag"

import {
  encodeLength,
  decodeLength
} from "@repository/lib/typescript/asn1/length"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

export function makeIntegerEncoder(ref: Ref<number>): () => number[] {

	let encode = (): number[] => {

    let bytes: number[] = []

		// encode value

		let valueBytes = function(ref: Ref<number>): number[] {

      // get value

      let value = ref.value

			let valueBytes = encodeInt(value)

			return valueBytes

		}(ref)

		// encode length

		let lengthBytes = encodeLength(valueBytes.length)

		// encode tag

		let tagBytes = encodeTag({
			class:  0,
			form:   0,
			number: 2,
		})

		bytes = [...bytes, ...tagBytes]
		bytes = [...bytes, ...lengthBytes]
		bytes = [...bytes, ...valueBytes]

		return bytes
	}

	return encode
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

export function makeIntegerDecoder(ref: Ref<number>): (bytes: number[]) => number {

  let bytesLength = 0

	let decode = (bytes: number[]): number => {

		// decode tag

		let [tag, tagBytesLength] = decodeTag(bytes.slice(bytesLength))

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		let [length, lengthBytesLength] = decodeLength(bytes.slice(bytesLength))

		bytesLength += lengthBytesLength

		// decode value

		let value = function(valueBytes: number[]): number {

			let value = decodeInt(valueBytes)

			return value

		}(bytes.slice(bytesLength, bytesLength + length))

		bytesLength += length

		// set value

		ref.value = value

		return bytesLength
	}

	return decode
}
