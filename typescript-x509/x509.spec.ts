import { generateKeyPairSync } from "crypto"

import {
  encodePKCS1PublicKey,
  decodePKCS1PublicKey,
  encodePKCS1PrivateKey,
  decodePKCS1PrivateKey
} from "@repository/lib/typescript/x509/x509"

describe("PKCS1", function () {

  describe("decodePKCS1PublicKey/encodePKCS1PublicKey", function () {

    it("should be able to encode and decode PKCS1 public key", function () {

      let { publicKey, privateKey } = generateKeyPairSync("rsa", {
        modulusLength: 4096,
        publicKeyEncoding: {
          type: "pkcs1",
          format: "der"
        },
        privateKeyEncoding: {
          type: "pkcs1",
          format: "der"
        }
      });

      let pkcs1PublicKey = decodePKCS1PublicKey(publicKey)

      expect(encodePKCS1PublicKey(pkcs1PublicKey)).toEqual(publicKey)
    })
  })

  describe("decodePKCS1PrivateKey/encodePKCS1PrivateKey", function () {

    it("should be able to encode and decode PKCS1 private key", function () {

      let { publicKey, privateKey } = generateKeyPairSync("rsa", {
        modulusLength: 4096,
        publicKeyEncoding: {
          type: "pkcs1",
          format: "der"
        },
        privateKeyEncoding: {
          type: "pkcs1",
          format: "der"
        }
      });

      let pkcs1PrivateKey = decodePKCS1PrivateKey(privateKey)

      expect(encodePKCS1PrivateKey(pkcs1PrivateKey)).toEqual(privateKey)
    })
  })
})
