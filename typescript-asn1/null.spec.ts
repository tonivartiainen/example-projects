import {
  makeNullEncoder,
  makeNullDecoder
} from "@repository/lib/typescript/asn1/null"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

describe("Null", function () {

  describe("encode/decode", function () {

    it("should be able to encode and decode null", function () {

      let input = new Ref<null>(null)

      let encode = makeNullEncoder(input)

      let bytes = encode()

      let output = new Ref<null>()

      let decode = makeNullDecoder(output)

      decode(bytes)

      expect(output).toEqual(input)
    })
  })
})
