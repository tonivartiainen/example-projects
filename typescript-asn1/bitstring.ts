import {
  encodeTag,
  decodeTag
} from "@repository/lib/typescript/asn1/tag"

import {
  encodeLength,
  decodeLength
} from "@repository/lib/typescript/asn1/length"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

export function makeBitStringEncoder(ref: Ref<string>): () => number[] {

	let encode = (): number[] => {

    let bytes: number[] = []

		// encode value

		let valueBytes = function(ref: Ref<string>): number[] {

      // get value

      let value = ref.value

      let valueBytes: number[] = []

			let subvalues = value.match(/[0,1]{1,8}/g)

			for (let i = 0; i < subvalues.length - 1; i++) {

				let subvalue = subvalues[i]

				let valueByte = parseInt(subvalue, 2)

				valueBytes = [...valueBytes, valueByte]
			}

			let subvalue = subvalues[subvalues.length - 1]

			let paddingByte = 8 - subvalue.length

			valueBytes = [paddingByte, ...valueBytes]

			subvalue = subvalue.padEnd(8, "0")

			let valueByte = parseInt(subvalue, 2)

			valueBytes = [...valueBytes, valueByte]

			return  valueBytes

		}(ref)

		// encode length

		let lengthBytes = encodeLength(valueBytes.length)

		// encode tag

		let tagBytes = encodeTag({
			class:  0,
			form:   0,
			number: 3,
		})

		bytes = [...bytes, ...tagBytes]
		bytes = [...bytes, ...lengthBytes]
		bytes = [...bytes, ...valueBytes]

		return bytes
	}

	return encode
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

export function makeBitStringDecoder(ref: Ref<string>): (bytes: number[]) => number {

  let bytesLength = 0

	let decode = (bytes: number[]): number => {

		// decode tag

		let [tag, tagBytesLength] = decodeTag(bytes.slice(bytesLength))

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		let [length, lengthBytesLength] = decodeLength(bytes.slice(bytesLength))

		bytesLength += lengthBytesLength

		// decode value

		let value = function(valueBytes: number[]): string {

      let value = ""

			let subvalues: string[] = []

			let valueByte = valueBytes[0]

			let padding = valueByte & 0xFF

			for (let i = 1; i < valueBytes.length - 1; i++) {

				let valueByte = valueBytes[i]

				let subvalue = valueByte.toString(2)

				subvalue = subvalue.padStart(8, "0")

        subvalues = [...subvalues, subvalue]
			}

			valueByte = valueBytes[valueBytes.length - 1]

			let subvalue = (valueByte >> padding).toString(2)

      subvalue = subvalue.padStart(8 - padding, "0");

			subvalues = [...subvalues, subvalue];

      value = subvalues.join("");

			return value

		}(bytes.slice(bytesLength, bytesLength + length))

		bytesLength += length

		// set value

		ref.value = value

		return bytesLength
	}

	return decode
}
