import {
  makeBitStringEncoder,
  makeBitStringDecoder
} from "@repository/lib/typescript/asn1/bitstring"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

describe("BitString", function () {

  describe("encode/decode", function () {

    it("should be able to encode and decode bit string", function () {

      let input = new Ref<string>("01010101010101010101")

      let encode = makeBitStringEncoder(input)

      let bytes = encode()

      let output = new Ref<string>()

      let decode = makeBitStringDecoder(output)

      decode(bytes)

      expect(output).toEqual(input)
    })
  })
})
