package jwk

import (
  "crypto/rand"
  "crypto/rsa"
	"math/big"
	"repository/lib/go/x509"
)

type JWKPublicKey struct {
	kty string
  use string
  key_ops string
  alg string
  kid string
  n big.Int
  e int
}

type JWKPrivateKey struct {
  kty string
  use string
  key_ops string
  alg string
  kid string
  v int
  n big.Int
  e int
  d big.Int
  p big.Int
  q big.Int
  dp big.Int
  dq big.Int
  qi big.Int
}

func DecodeJWKPublicKey(bytes []byte) (jwkPublicKey JWKPublicKey) {

  pkcs1PublicKey := x509.DecodePKCS1PublicKey(bytes)

  return JWKPublicKey{
    kty: "RSA",
    use: "sig",
    key_ops: "verify",
    alg: "RS256",
    n: pkcs1PublicKey.n,
    e: pkcs1PublicKey.e,
  }
}

func EncodeJWKPublicKey(jwkPublicKey JWKPublicKey) (bytes []byte) {

  pkcs1PublicKey := x509.PKCS1PublicKey{
    n: jwkPublicKey.n,
    e: jwkPublicKey.e,
  }

  return x509.EncodePKCS1PublicKey(pkcs1PublicKey)
}

func DecodeJWKPrivateKey(bytes []byte) (jwkPrivateKey JWKPrivateKey) {

  pkcs1PrivateKey := x509.DecodePKCS1PrivateKey(bytes)

  return JWKPrivateKey{
    kty: "RSA",
    use: "sig",
    key_ops: "verify",
    alg: "RS256",
    v: pkcs1PrivateKey.v,
    n: pkcs1PrivateKey.n,
    e: pkcs1PrivateKey.e,
    d: pkcs1PrivateKey.d,
    p: pkcs1PrivateKey.p,
    q: pkcs1PrivateKey.q,
    dp: pkcs1PrivateKey.dp,
    dq: pkcs1PrivateKey.dq,
    qi: pkcs1PrivateKey.qi,
  }
}

func EncodeJWKPrivateKey(jwkPrivateKey JWKPrivateKey) (bytes []byte) {

  pkcs1PrivateKey := PKCS1PrivateKey{
    v: jwkPrivateKey.v,
    n: jwkPrivateKey.n,
    e: jwkPrivateKey.e,
    d: jwkPrivateKey.d,
    p: jwkPrivateKey.p,
    q: jwkPrivateKey.q,
    dp: jwkPrivateKey.dp,
    dq: jwkPrivateKey.dq,
    qi: jwkPrivateKey.qi,
  }

  return x509.EncodePKCS1PrivateKey(pkcs1PrivateKey)
}

func GenerateJWKKeyPair() interface{ jwkPublicKey JWKPublicKey, jwkPrivateKey JWKPrivateKey } {

  let { publicKey, privateKey } = generateKeyPairSync("rsa", {
    modulusLength: 4096,
    publicKeyEncoding: {
      type: "pkcs1",
      format: "der"
    },
    privateKeyEncoding: {
      type: "pkcs1",
      format: "der"
    }
  })

  return {
    jwkPublicKey: decodeJWKPublicKey(publicKey),
    jwkPrivateKey: decodeJWKPrivateKey(privateKey)
  }
}
