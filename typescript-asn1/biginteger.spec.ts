import {
  makeBigIntegerEncoder,
  makeBigIntegerDecoder
} from "@repository/lib/typescript/asn1/biginteger"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

describe("BigInteger", function () {

  describe("encode/decode", function () {

    it("should be able to encode and decode big integer", function () {

      let input = new Ref<bigint>(1234567890n)

      let encode = makeBigIntegerEncoder(input)

      let bytes = encode()

      let output = new Ref<bigint>()

      let decode = makeBigIntegerDecoder(output)

      decode(bytes)

      expect(output).toEqual(input)
    })
  })
})
