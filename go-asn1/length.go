package asn1

import (
	"repository/lib/go/binary"
)

func EncodeLength(length int) (lengthBytes []byte) {

	if length < 0x80 {

		lengthByte := byte(length & 0x7F)

		lengthBytes = append(lengthBytes, lengthByte)

	} else {

		lengthBytes = binary.EncodeUint(uint64(length))

		lengthBytesLengthByte := byte(0x80 | len(lengthBytes)&0x7F)

		lengthBytes = append([]byte{lengthBytesLengthByte}, lengthBytes...)
	}

	return
}

func DecodeLength(bytes []byte) (length int, lengthBytesLength int) {

	lengthByte := bytes[lengthBytesLength]

	lengthBytesLength++

	if lengthByte&0x80 == 0x80 {

		length = int(binary.DecodeUint(bytes[lengthBytesLength : lengthBytesLength+int(lengthByte)&0x7F]))

		lengthBytesLength += int(lengthByte) & 0x7F

	} else {

		length = int(lengthByte & 0x7F)
	}

	return
}
