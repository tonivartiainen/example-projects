import { get } from "http"

import {
  SignPrivateKeyInput,
  VerifyPublicKeyInput,
  createSign,
  createVerify
} from "crypto"

import {
  JWKPublicKey,
  JWKPrivateKey,
  encodeJWKPublicKey,
  encodeJWKPrivateKey
} from "@repository/lib/typescript/jwk/jwk"

export interface Header {
  alg: string;
  jku?: string;
  jwk?: JWKPublicKey;
  kid?: string;
  typ?: string;
  cty?: string;
  crit?: string;
}

export interface Payload {
  iss?: string;
  sub?: string;
  aud?: string;
  exp?: string;
  nbf?: string;
  iat?: string;
  jti?: string;
}

function replaceBigIntJSON(key, value) {

  if (typeof value === 'bigint') {

    return value.toString() + 'n'
  }

  return value
}

function reviveBigIntJSON(key, value) {

  if (typeof value === 'string' && /^\d+n$/.test(value)) {

    return BigInt(value.slice(0, -1))
  }

  return value
}

export function createToken(header: Header, payload: Payload, jwkPrivateKey: JWKPrivateKey): string {

  let encodedHeader = Buffer.from(JSON.stringify(header, replaceBigIntJSON)).toString("base64")

  let encodedPayload = Buffer.from(JSON.stringify(payload, replaceBigIntJSON)).toString("base64")

  let privateKeyInput: SignPrivateKeyInput = {
    key: encodeJWKPrivateKey(jwkPrivateKey),
    type: "pkcs1",
    format: "der"
  }

  let signature = createSign("sha256").update(`${encodedHeader}.${encodedPayload}`).sign(privateKeyInput)

  let encodedSignature = Buffer.from(signature).toString("base64")

  let token = `${encodedHeader}.${encodedPayload}.${encodedSignature}`

  return token
}

export function verifyToken(token: string): object {

  let { encodedHeader, encodedPayload, encodedSignature } = /^(?<encodedHeader>.+)\.(?<encodedPayload>.+)\.(?<encodedSignature>.+)$/.exec(token).groups

  let signature = Buffer.from(encodedSignature, "base64")

  let header = JSON.parse(Buffer.from(encodedHeader, "base64").toString(), reviveBigIntJSON)

  let payload = JSON.parse(Buffer.from(encodedPayload, "base64").toString(), reviveBigIntJSON)

  let jwkPublicKey = getJWKPublicKey(header)

  let publicKeyInput: VerifyPublicKeyInput = {
    key: encodeJWKPublicKey(jwkPublicKey),
    type: "pkcs1",
    format: "der"
  }

  let verified = createVerify("sha256").update(`${encodedHeader}.${encodedPayload}`).verify(publicKeyInput, signature)

  if (verified) {

    return payload
  }
}

function getJWKPublicKey(header: Header) {

  let publicKeys = []

  if (header.jku) {

    get(header.jku, response => {

      let data = ""

      response.on("data", chunk => data = data + chunk)

      response.on("end", () => {

        let publicKeySet = JSON.parse(data, reviveBigIntJSON)

        publicKeys = [...publicKeys, publicKeySet.keys]
      });
    })
    .on("error", error => {

      throw error
    })

    publicKeys = publicKeys.filter(publicKey => {

      publicKey.kid == header.kid
    })
  }

  if (header.jwk) {

    let publicKey = header.jwk

    publicKeys = [...publicKeys, publicKey]
  }

  return publicKeys[0]
}
