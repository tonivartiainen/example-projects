package asn1

import (
	"testing"
)

func TestNull(t *testing.T) {

	var in interface{} = nil

	encode := MakeNullEncoder(in)

	bytes := encode()

	var out interface{} = nil

	decode := MakeNullDecoder(&out)

	decode(bytes)

	if in != out {

		t.Errorf("Encode %t, Decode %t", in, out)
	}
}
