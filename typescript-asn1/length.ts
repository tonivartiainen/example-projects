import {
  encodeUint,
  decodeUint
} from "@repository/lib/typescript/binary/int"

export function encodeLength(length: number): number[] {

  let lengthBytes: number[] = []

	if (length < 0x80) {

		let lengthByte = length & 0x7F

		lengthBytes = [...lengthBytes, lengthByte]

	} else {

		lengthBytes = encodeUint(length)

		let lengthBytesLengthByte = 0x80 | lengthBytes.length & 0x7F

		lengthBytes = [lengthBytesLengthByte, ...lengthBytes]
	}

	return lengthBytes
}

export function decodeLength(bytes: number[]): [number, number] {

  let length = 0

  let lengthBytesLength = 0

	let lengthByte = bytes[lengthBytesLength]

	lengthBytesLength++

	if ((lengthByte & 0x80) == 0x80) {

		length = decodeUint(bytes.slice(lengthBytesLength, lengthBytesLength + lengthByte & 0x7F))

		lengthBytesLength += lengthByte & 0x7F

	} else {

		length = lengthByte & 0x7F
	}

	return [length, lengthBytesLength]
}
