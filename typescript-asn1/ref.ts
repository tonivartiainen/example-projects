export class Ref<T> {

  private t: T

  public constructor(value?: T) {
    this.t = value
  }

  public get value(): T {
    return this.t
  }

  public set value(value: T) {
    this.t = value
  }
}
