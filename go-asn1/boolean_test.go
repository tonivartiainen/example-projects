package asn1

import (
	"testing"
)

func TestBoolean(t *testing.T) {

	in := true

	encode := MakeBooleanEncoder(in)

	bytes := encode()

	out := false

	decode := MakeBooleanDecoder(&out)

	decode(bytes)

	if in != out {

		t.Errorf("Encode %t, Decode %t", in, out)
	}
}
