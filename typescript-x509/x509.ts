import {
  makeIntegerEncoder,
  makeIntegerDecoder
} from "@repository/lib/typescript/asn1/integer"

import {
  makeBigIntegerEncoder,
  makeBigIntegerDecoder
} from "@repository/lib/typescript/asn1/biginteger"

import {
  makeSequenceEncoder,
  makeSequenceDecoder
} from "@repository/lib/typescript/asn1/sequence"

import { Ref } from "@repository/lib/typescript/asn1/ref"

export interface PKCS1PublicKey {
	n: bigint
	e: number
}

export interface PKCS1PrivateKey {
  v: number
  n: bigint
  e: number
  d: bigint
  p: bigint
  q: bigint
  dp: bigint
  dq: bigint
  qi: bigint
}

export function encodePKCS1PublicKey(pkc1PublicKey: PKCS1PublicKey): Buffer {

  let ref = {
    n: new Ref<bigint>(pkc1PublicKey.n),
    e: new Ref<number>(pkc1PublicKey.e)
  }

	let encode = makeSequenceEncoder(
		makeBigIntegerEncoder(ref.n),
		makeIntegerEncoder(ref.e),
	)

	let bytes = encode()

	return Buffer.from(bytes)
}

export function decodePKCS1PublicKey(buffer: Buffer): PKCS1PublicKey {

	let ref = {
    n: new Ref<bigint>(),
    e: new Ref<number>(),
  }

	let decode = makeSequenceDecoder(
		makeBigIntegerDecoder(ref.n),
		makeIntegerDecoder(ref.e),
	)

	decode([...buffer])

  let pkcs1PublicKey: PKCS1PublicKey = {
    n: ref.n.value,
    e: ref.e.value
  }

	return pkcs1PublicKey
}

export function encodePKCS1PrivateKey(pkcs1PrivateKey: PKCS1PrivateKey): Buffer {

  let ref = {
    v: new Ref<number>(pkcs1PrivateKey.v),
    n: new Ref<bigint>(pkcs1PrivateKey.n),
    e: new Ref<number>(pkcs1PrivateKey.e),
    d: new Ref<bigint>(pkcs1PrivateKey.d),
    p: new Ref<bigint>(pkcs1PrivateKey.p),
    q: new Ref<bigint>(pkcs1PrivateKey.q),
    dp: new Ref<bigint>(pkcs1PrivateKey.dp),
    dq: new Ref<bigint>(pkcs1PrivateKey.dq),
    qi: new Ref<bigint>(pkcs1PrivateKey.qi)
  }

	let encode = makeSequenceEncoder(
    makeIntegerEncoder(ref.v),
		makeBigIntegerEncoder(ref.n),
		makeIntegerEncoder(ref.e),
    makeBigIntegerEncoder(ref.d),
    makeBigIntegerEncoder(ref.p),
    makeBigIntegerEncoder(ref.q),
    makeBigIntegerEncoder(ref.dp),
    makeBigIntegerEncoder(ref.dq),
    makeBigIntegerEncoder(ref.qi)
	)

	let bytes = encode()

	return Buffer.from(bytes)
}

export function decodePKCS1PrivateKey(buffer: Buffer): PKCS1PrivateKey {

	let ref = {
    v: new Ref<number>(),
    n: new Ref<bigint>(),
    e: new Ref<number>(),
    d: new Ref<bigint>(),
    p: new Ref<bigint>(),
    q: new Ref<bigint>(),
    dp: new Ref<bigint>(),
    dq: new Ref<bigint>(),
    qi: new Ref<bigint>()
  }

	let decode = makeSequenceDecoder(
    makeIntegerDecoder(ref.v),
		makeBigIntegerDecoder(ref.n),
		makeIntegerDecoder(ref.e),
    makeBigIntegerDecoder(ref.d),
    makeBigIntegerDecoder(ref.p),
    makeBigIntegerDecoder(ref.q),
    makeBigIntegerDecoder(ref.dp),
    makeBigIntegerDecoder(ref.dq),
    makeBigIntegerDecoder(ref.qi)
	)

	decode([...buffer])

  let pkcs1PrivateKey: PKCS1PrivateKey = {
    v: ref.v.value,
    n: ref.n.value,
    e: ref.e.value,
    d: ref.d.value,
    p: ref.p.value,
    q: ref.q.value,
    dp: ref.dp.value,
    dq: ref.dq.value,
    qi: ref.qi.value
  }

	return pkcs1PrivateKey
}
