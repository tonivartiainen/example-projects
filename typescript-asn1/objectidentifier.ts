import {
  encodeVaruint,
  decodeVaruint
} from "@repository/lib/typescript/binary/int"

import {
  encodeTag,
  decodeTag
} from "@repository/lib/typescript/asn1/tag"

import {
  encodeLength,
  decodeLength
} from "@repository/lib/typescript/asn1/length"

import {
  Ref
} from "@repository/lib/typescript/asn1/ref"

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

export function makeObjectIdentifierEncoder(ref: Ref<string>): () => number[] {

	let encode = (): number[] => {

    let bytes: number[] = []

		// encode value

		let valueBytes = function(ref: Ref<string>): number[] {

      // get value

      let value = ref.value

      let valueBytes: number[] = []

			let subvalues = value.split(".")

			let valueByte = 0

			let subvalue = subvalues[0]

			let parsedSubvalue = parseInt(subvalue, 10)

			valueByte += parsedSubvalue * 40

			subvalue = subvalues[1]

			parsedSubvalue = parseInt(subvalue, 10)

			valueByte += parsedSubvalue

			valueBytes = [...valueBytes, valueByte]

			for (let i = 2; i < subvalues.length; i++) {

				subvalue = subvalues[i]

				parsedSubvalue = parseInt(subvalue, 10)

				let subvalueBytes = encodeVaruint(parsedSubvalue)

				valueBytes = [...valueBytes, ...subvalueBytes]
			}

			return valueBytes

		}(ref)

		// encode length

		let lengthBytes = encodeLength(valueBytes.length)

		// encode tag

		let tagBytes = encodeTag({
			class:  0,
			form:   0,
			number: 6,
		})

		bytes = [...bytes, ...tagBytes]
		bytes = [...bytes, ...lengthBytes]
		bytes = [...bytes, ...valueBytes]

		return bytes
	}

	return encode
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

export function makeObjectIdentifierDecoder(ref: Ref<string>): (bytes: number[]) => number {

	let bytesLength = 0

	let decode = (bytes: number[]): number => {

		// decode tag

		let [tag, tagBytesLength] = decodeTag(bytes.slice(bytesLength))

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		let [length, lengthBytesLength] = decodeLength(bytes.slice(bytesLength))

		bytesLength += lengthBytesLength

		// decode value

		let value = function(valueBytes: number[]): string {

      let value = ""

			let subvalues: string[] = []

			let valueByte = valueBytes[0]

			let subvalue = (valueByte / 40 - valueByte % 40 / 40).toString()

			subvalues = [...subvalues, subvalue];

			subvalue = (valueByte % 40).toString()

			subvalues = [...subvalues, subvalue];

			for (let i = 1; i < valueBytes.length; i++) {

				let [decodedSubvalue, decodedSubvalueBytesLength] = decodeVaruint(valueBytes.slice(i))

				subvalue = decodedSubvalue.toString()

				i += decodedSubvalueBytesLength - 1

				subvalues = [...subvalues, subvalue];
			}

			value = subvalues.join(".")

			return value

		}(bytes.slice(bytesLength, bytesLength + length))

		bytesLength += length

		// set value

		ref.value = value

		return bytesLength
	}

	return decode
}
