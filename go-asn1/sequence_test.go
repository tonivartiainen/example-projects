package asn1

import (
	"testing"
)

type PKCS1PublicKey struct {
	n int64
	e int64
}

func TestSequence(t *testing.T) {

	in := PKCS1PublicKey{
		n: 100,
		e: 200,
	}

	encode := MakeSequenceEncoder(
		MakeIntegerEncoder(in.n),
		MakeIntegerEncoder(in.e),
	)

	bytes := encode()

	out := PKCS1PublicKey{}

	decode := MakeSequenceDecoder(
		MakeIntegerDecoder(&out.n),
		MakeIntegerDecoder(&out.e),
	)

	decode(bytes)

	if in != out {

		t.Errorf("Encode %v, Decode %v", in, out)
	}
}
