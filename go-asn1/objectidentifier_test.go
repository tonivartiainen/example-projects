package asn1

import (
	"testing"
)

func TestObjectIdentifier(t *testing.T) {

	in := "1.2.840.113549.1.1"

	encode := MakeObjectIdentifierEncoder(in)

	bytes := encode()

	out := ""

	decode := MakeObjectIdentifierDecoder(&out)

	decode(bytes)

	if in != out {

		t.Errorf("Encode %s, Decode %s", in, out)
	}
}
