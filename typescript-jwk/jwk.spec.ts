import { generateKeyPairSync } from "crypto"

import {
  encodeJWKPublicKey,
  decodeJWKPublicKey,
  encodeJWKPrivateKey,
  decodeJWKPrivateKey,
  generateJWKKeyPair
} from "@repository/lib/typescript/jwk/jwk"


describe("JWK", function () {

  describe("encodeJWKPublicKey/decodeJWKPublicKey", function () {

    it("should be able to encode and decode PKCS1 public key", function () {

      let { publicKey, privateKey } = generateKeyPairSync("rsa", {
        modulusLength: 4096,
        publicKeyEncoding: {
          type: "pkcs1",
          format: "der"
        },
        privateKeyEncoding: {
          type: "pkcs1",
          format: "der"
        }
      })

      let jwkPublicKey = decodeJWKPublicKey(publicKey)

      expect(encodeJWKPublicKey(jwkPublicKey)).toEqual(publicKey)
    })

    it("should be able to encode and decode JWK public key", function () {

      let { jwkPublicKey, jwkPrivateKey } = generateJWKKeyPair()

      let bytes = encodeJWKPublicKey(jwkPublicKey)

      expect(decodeJWKPublicKey(bytes)).toEqual(jwkPublicKey)
    })
  })

  describe("encodeJWKPrivateKey/decodeJWKPrivateKey", function () {

    it("should be able to encode and decode PKCS1 private key", function () {

      let { publicKey, privateKey } = generateKeyPairSync("rsa", {
        modulusLength: 4096,
        publicKeyEncoding: {
          type: "pkcs1",
          format: "der"
        },
        privateKeyEncoding: {
          type: "pkcs1",
          format: "der"
        }
      })

      let jwkPrivateKey = decodeJWKPrivateKey(privateKey)

      expect(encodeJWKPrivateKey(jwkPrivateKey)).toEqual(privateKey)
    })

    it("should be able to encode and decode JWK private key", function () {

      let { jwkPublicKey, jwkPrivateKey } = generateJWKKeyPair()

      let bytes = encodeJWKPrivateKey(jwkPrivateKey)

      expect(decodeJWKPrivateKey(bytes)).toEqual(jwkPrivateKey)
    })
  })
})
