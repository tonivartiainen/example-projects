import {
  encodeTag,
  decodeTag
} from "@repository/lib/typescript/asn1/tag"

import {
  encodeLength,
  decodeLength
} from "@repository/lib/typescript/asn1/length"

/********************************************************************************
*
* ENCODER
*
********************************************************************************/

export function makeSequenceEncoder(...sequence: {(): number[]}[]): () => number[] {

	let encode = (): number[] => {

    let bytes: number[] = []

		// encode value

		let valueBytes = function(value: {(): number[]}[]): number[] {

      let valueBytes: number[] = []

			for (let encode of value) {

				valueBytes = [...valueBytes, ...encode()]
			}

			return valueBytes

		}(sequence)

		// encode length

		let lengthBytes = encodeLength(valueBytes.length)

		// encode tag

		let tagBytes = encodeTag({
			class:  0,
			form:   32,
			number: 16,
		})

		bytes = [...bytes, ...tagBytes]
		bytes = [...bytes, ...lengthBytes]
		bytes = [...bytes, ...valueBytes]

		return bytes
	}

	return encode
}

/********************************************************************************
*
* DECODER
*
********************************************************************************/

export function makeSequenceDecoder(...sequence: {(bytes: number[]): number}[]): (bytes: number[]) => number {

  let bytesLength = 0

	let decode = (bytes: number[]): number => {

		// decode tag

		let [tag, tagBytesLength] = decodeTag(bytes.slice(bytesLength))

		bytesLength += tagBytesLength

		// TODO: validate tag

		// decode length

		let [length, lengthBytesLength] = decodeLength(bytes.slice(bytesLength))

		bytesLength += lengthBytesLength

		// decode value

		let value = function(valueBytes: number[]) {

			let bytesLength = 0

			for (let decode of sequence) {

				bytesLength += decode(valueBytes.slice(bytesLength))
			}

		}(bytes.slice(bytesLength, bytesLength + length))

		bytesLength += length

		return bytesLength
	}

	return decode
}
