package x509

import (
	"testing"
)

func TestPKCS1PublicKey(t *testing.T) {

	in := PKCS1PublicKey{
		n: 100,
		e: 200,
	}

	bytes := EncodePKCS1PublicKey(in)

	out := DecodePKCS1PublicKey(bytes)

	if in != out {

		t.Errorf("Encode %v, Decode %v", in, out)
	}
}
